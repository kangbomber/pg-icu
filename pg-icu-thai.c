#include "postgres.h"
#include <string.h>
#include "fmgr.h"
#include "utils/geo_decls.h"

//#include "unicode/ustdio.h"
#include "unicode/utypes.h"
#include "unicode/utext.h"
#include "unicode/ubrk.h"

PG_MODULE_MAGIC;

///* by value */

PG_FUNCTION_INFO_V1(add_one);

Datum
add_one(PG_FUNCTION_ARGS)
{
  ereport(ERROR,
          (errcode(ERRCODE_INTERNAL_ERROR),
           errmsg("error calling ubrk_open()")));
  int32 arg = PG_GETARG_INT32(0);

  PG_RETURN_INT32(arg + 1);
}

/*
 * types
 */

/* self-defined type */
typedef struct {
  char    *buffer; /* text to parse */
  int     len;    /* length of the text in buffer */
  int     pos;    /* position of the parser */
  UText   *ut;
  UBreakIterator *bi;
} ParserState;

/* copy-paste from wparser.h of tsearch2 */
typedef struct {
  int     lexid;
  char    *alias;
  char    *descr;
} LexDescr;

/*
 * prototypes
 */
PG_FUNCTION_INFO_V1(icuprs_start);
Datum icuprs_start(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(icuprs_getlexeme);
Datum icuprs_getlexeme(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(icuprs_end);
Datum icuprs_end(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(icuprs_lextype);
Datum icuprs_lextype(PG_FUNCTION_ARGS);

/*
 * functions
 */
Datum icuprs_start(PG_FUNCTION_ARGS)
{
  ParserState *pst = (ParserState *) palloc(sizeof(ParserState));
  pst->buffer = (char *) PG_GETARG_POINTER(0);

  UErrorCode err = U_ZERO_ERROR;

  UBreakIterator *bi = ubrk_open(UBRK_WORD, 0, NULL, -1, &err);
  if (U_FAILURE(err)) {
    // error handle
    ubrk_close(bi);
    ereport(ERROR,
            (errcode(ERRCODE_INTERNAL_ERROR),
             errmsg("error calling ubrk_open()")));
  }
  pst->bi = bi;

  char *text = (char *) PG_GETARG_POINTER(0);
  UText *ut = utext_openUTF8(NULL, text, -1, &err);
  if (U_FAILURE(err)) {
    // error handle
    ubrk_close(bi);
    utext_close(ut);
    ereport(ERROR,
            (errcode(ERRCODE_INTERNAL_ERROR),
             errmsg("error calling utext_openUTF8()")));
  }
  pst->ut = ut;

  ubrk_setUText(bi, ut, &err);
  if (U_FAILURE(err)) {
    // error handle
    ubrk_close(bi);
    utext_close(ut);
    ereport(ERROR,
            (errcode(ERRCODE_INTERNAL_ERROR),
             errmsg("error calling ubrk_setUText()")));
  }

  pst->len = PG_GETARG_INT32(1);
  pst->pos = ubrk_first(bi); // 0
  //int32_t p = ubrk_first(bi);

  PG_RETURN_POINTER(pst);
}

Datum icuprs_getlexeme(PG_FUNCTION_ARGS)
{
  ParserState *pst   = (ParserState *) PG_GETARG_POINTER(0);
  char        **t    = (char **) PG_GETARG_POINTER(1);
  int         *tlen  = (int *) PG_GETARG_POINTER(2);
  int         type;
  int         newpos;

  //*tlen = pst->pos;
  *t = pst->buffer +  pst->pos;

  //if ((pst->buffer)[pst->pos] == ' ') {
  //  /* blank type */
  //  type = 12;
  //  /* go to the next non-white-space character */
  //  while (((pst->buffer)[pst->pos] == ' ') && (pst->pos < pst->len)) {
  //    (pst->pos)++;
  //  }
  //} else {
  //  /* word type */
  //  type = 3;
  //  /* go to the next white-space character */
  //  while (((pst->buffer)[pst->pos] != ' ') && (pst->pos < pst->len)) {
  //    (pst->pos)++;
  //  }
  //}
  type = 3; // word type
  // space and punctuations see ascii 7bits
  unsigned char firstbyte = *(pst->buffer + pst->pos);
  if (firstbyte > 127) {

    type = 4; // non-ascii
  }
  else if ((firstbyte <= 47) ||
           ((firstbyte >= 58) && (firstbyte <= 64)) ||
           ((firstbyte >= 91) && (firstbyte <= 96)) ||
           (firstbyte >= 123)) {

    type = 12; // space
  }


  newpos = ubrk_next(pst->bi);

  *tlen = newpos - pst->pos;

  pst->pos = newpos;

  /* we are finished if (*tlen == 0) */
  //if (*tlen == 0) type=0;
  if (newpos == UBRK_DONE) {
    type = 0;
    *tlen = 0;
  }

  PG_RETURN_INT32(type);
}

Datum icuprs_end(PG_FUNCTION_ARGS)
{
  ParserState *pst = (ParserState *) PG_GETARG_POINTER(0);

  ubrk_close(pst->bi);
  utext_close(pst->ut);

  pfree(pst);

  PG_RETURN_VOID();
}

Datum icuprs_lextype(PG_FUNCTION_ARGS)
{
  /*
    Remarks:
    - we have to return the blanks for headline reason
    - we use the same lexids like Teodor in the default
      word parser; in this way we can reuse the headline
      function of the default word parser.
  */
  LexDescr *descr = (LexDescr *) palloc(sizeof(LexDescr) * (3+1));

  /* there are only two types in this parser */
  descr[0].lexid = 3;
  descr[0].alias = pstrdup("word");
  descr[0].descr = pstrdup("Word");

  descr[1].lexid = 4;
  descr[1].alias = pstrdup("non_ascii");
  descr[1].descr = pstrdup("non_ascii");

  descr[2].lexid = 12;
  descr[2].alias = pstrdup("blank");
  descr[2].descr = pstrdup("Space symbols");

  descr[3].lexid = 0;

  PG_RETURN_POINTER(descr);
}

