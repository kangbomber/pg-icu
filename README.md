# PG-ICU
    I wanted to implement full text search and indexing Thai in my-app but it turned out that postgres didn't really support Thai. I did my research and found that I could make a postgres plugin to accomplish this which utilized an existing project called ICU or icu4c. It turned out that this does not only support Thai but also a bunch of other languages including Khmer, Laos, and more thanks to ICU.

## Supported languages
    - Thai
    - English
    - Lao
    - Khmer
    - More 

## Install postgresql and dependencies
I only tested this only on Ubuntu 20.04 LTS

    $ sudo apt-get install postgresql-12
    $ sudo apt-get install postgresql-server-dev-12
    $ sudo apt-get install libicu-dev
    $ sudo apt install build-essential

## Install this plugin to your postgres
first build from source

    $ git clone https://gitlab.com/kangbomber/pg-icu
    $ cd pg-icu
    $ make
    $ sudo make install

then create postgres extension with psql

    $ sudo -u postgres psql
    postgres=# create extension "pg-icu-thai";

    # the installation is complete
    # it's time to test and see if it works

    postgres=# select * from ts_parse('icuparser', 'อยากไปเที่ยว. I thought a though.');
     tokid |  token
    -------+---------
         4 | อยาก
         4 | ไป
         4 | เที่ยว
        12 | .
        12 |
         3 | I
        12 |
         3 | thought
        12 |
         3 | a
        12 |
         3 | though
        12 | .
    (13 rows)

    postgres=# select * from to_tsvector('icu', 'อยากไปเที่ยว. She loves an elephant');
                     to_tsvector
    ---------------------------------------------
     'eleph':7 'love':5 'อยาก':1 'เที่ยว':3 'ไป':2
    (1 row)