MODULES = pg-icu-thai
EXTENSION = pg-icu-thai
DATA = pg-icu-thai--1.0.0.sql

PGFILEDESC = "make fulltext search in Thai possible"

#icu_cflags := $(shell pkg-config --cflags icu-io icu-uc icu-i18n)
icu_libs := $(shell pkg-config --libs icu-io icu-uc icu-i18n)
pg_libs := $(shell pg_config --libs)
pg_libdir := -L$(shell pg_config --pkglibdir)

#PG_CFLAGS = $(icu_cflags)
#PG_CPPFLAGS = $(icu_cflags)
#PG_LDFLAGS = $(icu_libs) $(pg_libdir) -lpgcommon -lpgport -Wl,-z,defs
PG_LDFLAGS = $(icu_libs)

PGXS := $(shell pg_config --pgxs)
include $(PGXS)
