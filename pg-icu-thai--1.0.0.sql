CREATE OR REPLACE FUNCTION
add_one(int) RETURNS int AS 'MODULE_PATHNAME','add_one'
LANGUAGE C STRICT;

CREATE FUNCTION icuprs_start(internal,int4)
RETURNS internal
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

CREATE FUNCTION icuprs_getlexeme(internal,internal,internal)
RETURNS internal
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

CREATE FUNCTION icuprs_end(internal)
RETURNS void
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

CREATE FUNCTION icuprs_lextype(internal)
RETURNS internal
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

CREATE TEXT SEARCH PARSER icuparser (
        START     = 'icuprs_start',
        GETTOKEN  = 'icuprs_getlexeme',
        END       = 'icuprs_end',
        LEXTYPES  = 'icuprs_lextype'
);

CREATE TEXT SEARCH CONFIGURATION icu (
       PARSER = icuparser
);

ALTER TEXT SEARCH CONFIGURATION icu
ADD MAPPING FOR word WITH english_stem;

ALTER TEXT SEARCH CONFIGURATION icu
ADD MAPPING FOR non_ascii WITH simple;

-- CREATE FULLTEXT CONFIGURATION  testcfg  PARSER  'testparser' LOCALE  NULL;
-- CREATE FULLTEXT MAPPING ON testcfg FOR word WITH simple;
