#include <stdio.h>
#include "unicode/utypes.h"
#include "unicode/utext.h"
#include "unicode/ubrk.h"

int main() {
  const char *normal_str = "he's too cool";
  UErrorCode err = U_ZERO_ERROR;
  UText *ut = utext_openUTF8(NULL, normal_str, -1, &err);
  if (U_FAILURE(err)) {
    printf("u_failure\n");
    return 1;
  }

  UBreakIterator *bi = ubrk_open(UBRK_WORD, 0, NULL, -1, &err);
  if (U_FAILURE(err)) {
    printf("u_failure\n");
    return 1;
  }

  ubrk_setUText(bi, ut, &err);
  if (U_FAILURE(err)) {
    printf("u_failure\n");
    return 1;
  }

  int32_t p = ubrk_first(bi);
  while (p != UBRK_DONE) {
    printf("Boundary at %d\n", p);
    p = ubrk_next(bi);
  }

  ubrk_close(bi);
  utext_close(ut);

  return 0;
}
